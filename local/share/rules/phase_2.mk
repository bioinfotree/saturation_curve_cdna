### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:54:29 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

MAKE = bmake
# number of replicas to be made
REPS_NUM ?= 10




# Generate directory list as external rule
dirs_lst.mk:
	echo "Prepare directories list..."; \
	( \
		echo -n 'DIRS_LST := ' ; \
		SEQS=`seq 1 1 $(REPS_NUM) | tr '\n' ' '` ; \
		for i in $$SEQS; do \
		(\
			echo -n "reply_$$i " ; \
		)\
		done; \
	) >$@

include dirs_lst.mk






# create dirs and link makefiles an rules phase_2.1.mk on
# each of them
.PHONY: dirs_prepare
dirs_prepare: rules.mk makefile
	_comment () { echo -n ""; }; \
	for d in $(DIRS_LST); do \
	( \
	_comment "create the directory"; \
	mkdir -p $$d; \
	_comment "makefile full path"; \
	MAKEFILE_PATH=`readlink -f $^2`; \
	_comment "ruless full path"; \
	RULES_PATH=`readlink -f $<`; \
	DIR_PATH=`dirname $$RULES_PATH`; \
	FILENAME=`basename $$RULES_PATH`; \
	EXTENSION=$${FILENAME##*.}; \
	FILENAME=$${FILENAME%.*}; \
	TARGHET_RULES_PATH="$$DIR_PATH/$$FILENAME.1.$$EXTENSION"; \
	cd $$d; \
	_comment "link new phase"; \
	ln -sf $$TARGHET_RULES_PATH $<; \
	echo "Copied $$TARGHET_RULES_PATH into $$d.."; \
	_comment "link makefile"; \
	ln -sf $$MAKEFILE_PATH $^2; \
	echo "Copied $$MAKEFILE_PATH into $$d.."; \
	); \
	done


# execute bmake on each of them
subsystem: dirs_prepare
	@echo Looking into subdirs: $(MAKE)
	-for d in $(DIRS_LST); do (cd $$d; $(MAKE) ); done



.PHONY: test
test:
	@echo $(DIRS_LST)


# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += dirs_prepare \
	 subsystem



# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=



# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += dirs_lst.mk




# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:
	@echo cleaning up in .
	-for d in $(DIRS_LST); do (cd $$d; $(MAKE) clean ); done
	-for d in $(DIRS_LST); do ( $(RM) -rf $$d; ); done



######################################################################
### phase_2.mk ends here
