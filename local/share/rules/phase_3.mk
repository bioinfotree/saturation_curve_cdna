### phase_3.mk --- 
## 
## Filename: phase_3.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Thu Aug  4 14:36:57 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:



# I need err_handler.R fit_nl_model.R plot_nl_model.R
context prj/saturation_curve

# make variable DIRS_LST available
extern ../phase_2/dirs_lst.mk as DIRS_LST_RULE
include $(DIRS_LST_RULE)


# "extern" files in FILE_LST from every reply of phase_2
# Make variables SATURATION_TABLE and NL_MODEL_PARAMETERS_PLUS availables
# that contains lists of aliases for extern files
FILE_LST := saturation_table.txt nl_model_parameters_plus.txt
external_lst.mk: $(DIRS_LST_RULE)
	echo "Prepare external lists...\n"; \
	( \
	DIRS_LST='$(DIRS_LST)'; \
	FILE_LST='$(FILE_LST)'; \
	for FILE in $$FILE_LST; do \
	( \
	FILENAME=`basename $$FILE`; \
	EXTENSION=$${FILENAME##*.}; \
	PREFIX=$${FILENAME%.*}; \
	for DIR in $$DIRS_LST; do \
	( \
	echo "extern ../phase_2/$$DIR/$$FILENAME as $$PREFIX"_"$$DIR"; \
	) \
	done; \
	echo; \
	echo -n $$PREFIX | tr '[:lower:]' '[:upper:]'; \
	echo -n " :="; \
	for DIR in $$DIRS_LST; do \
	( \
	echo -n " \$$($$PREFIX"_"$$DIR)"; \
	) \
	done; \
	echo -e "\n"; \
	) \
	done; \
	) > $@

include external_lst.mk



# SATURATION_TABLE define by external_lst.mk
# this rule also generate saturation_table_reply_%_err.txt files

# define mean values for unique_transcripts_found from corresponding values of 
# unique_transcripts_found, from all files of the list $(SATURATION_TABLE).
# Calculates the error of the average values of unique_transcripts_found 
# found computationally.
saturation_table_mean_err.txt: $(SATURATION_TABLE)
	err_handler -r $(SATURATION_TABLE) \
	> $@


# NL_MODEL_PARAMETERS_PLUS define by external_lst.mk
PARAMETERS_FILE = nl_model_parameters_mean_err.txt
# fit model on mean values and returns parameters a and b of the model
fitted_table_mean_err.txt: saturation_table_mean_err.txt
	paste $< \
	<(fit_nl_model --a-param $(NLM_A_PARAM) --b-param $(NLM_B_PARAM) --param-out-file $(PARAMETERS_FILE) \
	--row-names 1,2 < $< \
	| cut -f 3 \
	| awk 'NR==1{printf "fitted_unique_transcripts_found"}1') > $@

$(PARAMETERS_FILE): fitted_table_mean_err.txt
	touch $<




# plot mean values, fitted model on mean values and error bars
SATURATION_PLOT_MEAN_ERR ?= saturation_plot_mean_err.pdf
$(SATURATION_PLOT_MEAN_ERR): fitted_table_mean_err.txt nl_model_parameters_mean_err.txt
	plot_nl_model -o $@ -s -t "saturation curve" -x "reads sampled [#]" -y "different transcripts identified [#]" -q -d "$(PLOT_DATA_LEGEND)","model: " -c "blue","red" -p $^2,$^2 --in-err-file $<,$< 1,2 1,7 < $<






# fitted_table_reply_%_err.txt list
FITTED_TABLE_REPLY_ERR = $(addsuffix _err.txt, $(addprefix fitted_table_, $(DIRS_LST)))

# Test if prerequisite file from phase_2 exist and is readable. Else call bmake
# Takes the file "fitted_table" of a particular replica. 
# Select the data "# list_file_name	simple_size	unique_transcripts_found".
# Append data for "sd	var	ci_up_limit	ci_low_limit"  from file "saturation_table_mean_err.txt". 
# Append "fitted_unique_transcripts_found" from file "fitted_table". 
# Edit the header by removing the first field "# list_file_name".
fitted_table_reply_%_err.txt: ../phase_2/reply_%/fitted_table.txt saturation_table_mean_err.txt 
	if [ ! -r $< ]; then \
	bmake $<; \
	fi && \
	paste \
	<(cut -f 1,2,3 $<) \
	<(bawk 'NR==1{printf "# list_file_name\t"}1' $^2 | cut --complement -f 1,2,3) \
	<(cut --complement -f 1,2,3 $<) \
	| awk '!/^[$$]/ { \
	if (FNR==1) \
	{ \
	for(i=3; i<NF; i++) { printf("%s\t", $$i); } \
	printf "%s\n", $$NF; \
	} \
	else { print $$0; } \
	}' > $@


# assemble name saturation_plot_reply_%_err.pdf list
SATURATION_PLOT_REPLY_ERR = $(addsuffix _err.pdf, $(addprefix saturation_plot_, $(DIRS_LST)))

# Test if prerequisite file from phase_2 exist and is readable. Else call bmake
# plot the same graph as in the given replicates, with fitted model, and add error bars
saturation_plot_reply_%_err.pdf: fitted_table_reply_%_err.txt ../phase_2/reply_%/nl_model_parameters.txt
	if [ ! -r $^2 ]; then \
	bmake $^2; \
	fi && \
	plot_nl_model -o $@ -s -t "saturation curve" -x "reads sampled [#]" -y "different transcripts identified [#]" -q -d "$(PLOT_DATA_LEGEND)","model: " -c "blue","red" -p $^2,$^2 --in-err-file $<,$< 1,2 1,7 < $<



nl_model_parameters_plus.txt: $(NL_MODEL_PARAMETERS_PLUS)
	paste \
	<(echo "reply $+" | tr ' ' '\n') \
	<(cat $+ \
	|  awk '!/^[$$]/ { \
	if (NR == 1) {print $$0;} \
	else if ($$0 ~ /[0-9]/) {print $$0;} \
	}') > $@


# test
.PHONY: test_new_function
test_new_function:
	@echo $(SATURATION_PLOT_REPLY_ERR)


# Standard Phony Targets for Users.
ALL   += saturation_table_mean_err.txt \
	 fitted_table_mean_err.txt \
	 nl_model_parameters_mean_err.txt \
	 $(SATURATION_PLOT_MEAN_ERR) \
	 $(FITTED_TABLE_REPLY_ERR) \
	 $(SATURATION_PLOT_REPLY_ERR) \
	 nl_model_parameters_plus.txt


# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += external_lst.mk \
	 saturation_table_mean_err.txt \
	 fitted_table_mean_err.txt \
	 nl_model_parameters_mean_err.txt \
	 $(SATURATION_PLOT_MEAN_ERR) \
	 $(FITTED_TABLE_REPLY_ERR) \
	 $(SATURATION_PLOT_REPLY_ERR) \
	 nl_model_parameters_plus.txt


# Declare clean as
# phony targhet
.PHONY: clean
# so i can recall function to
# delete dir or other files
clean:

######################################################################
### phase_3.mk ends here
